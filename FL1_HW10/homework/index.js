//Task 1
function isEquals(a, b){
    return a === b;
}
//Task 2
function numberToString(some){
    return String(some);
}
//Task 3
function storeNames(){
    let arr = [];
    for(let i = 0; i < arguments.length; i++){
        arr[i] = arguments[i];
    }
    return arr;
}
//Task 4
function getDivision(a, b){
    let result;
    result = a > b ? a / b : b / a;
    return result;
}
//Task 5
function negativeCount(some) {
    let result = 0;
    for(let i = 0; i < some.length; i++){
        result = some[i] < 0 ? result = result + 1 : result;
    }

    return result;
}
//Task 6
function letterCount(a, b){
    let result = 0;
    for(let i = 0; i < a.length; i++){
        result = a[i] === b ? result = result + 1 : result;
    }

    return result;
}
//Task 7
function countPoints(some){
    let result = 0;
    let index;
    let x = '';
    let y = '';
    let winnerPoint = 3;
    let drawPoint = 1;

    for(let i = 0; i < some.length; i++){
        index = some[i].indexOf(':');
        x = some[i].substr(0, index);
        y = some[i].substr(index + 1);

        if(parseInt(x) > parseInt(y)){
            result = result + winnerPoint;
        }else if(parseInt(x) === parseInt(y)){
            result = result + drawPoint;
        }else{
            result;
        }
    }

    return result;
}