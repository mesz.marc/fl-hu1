// TASK 1.
function getAge(some){
    let date1 = some;
    let date1_year = date1.getFullYear();
    let date1_month = date1.getMonth();
    let date1_day = date1.getDate();

    let date2 = new Date();
    let date2_year = date2.getFullYear();
    let date2_month = date2.getMonth() + 1;
    let date2_day = date2.getDate();

    let result = date2_year - date1_year;

    if(date1_month >= date2_month && date1_day > date2_day){
        result = result - 1;
    }

    return result;
}

// TASK 2.
function getWeekDay(some){
    let date1 = new Date(some);
    let day = date1.getDay();
    let result = '';
    switch(day){
        case 1:
            result = 'Monday';
            break;
        case 2:
            result = 'Tuesday';
            break;
        case 3:
            result = 'Wednesday';
            break;
        case 4:
            result = 'Thursday';
            break;
        case 5:
            result = 'Friday';
            break;
        case 6:
            result = 'Saturday';
            break;
        case 0:
            result = 'Sunday';
            break;
        default:
            result = 'Sunday';
    }

     return result;
}

// TASK 3.
function getProgrammersDay(some) {
    let monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    let year = some;
    let month = 8;
    let day = 13;
    if(year % 4 === 0){
        day = 12;
    }
    let day_name = getWeekDay(new Date(year, month, day));
    let result = day + ' ' + monthNames[month] + ', ' + year + ' (' + day_name + ')';

    return result;
}

// TASK 4.
function howFarIs(some){
    let day_name = some.toLowerCase();
    let days = ['Sunday', 'monday', 'tuesday', 'wednesday', 'thursday',
                'friday', 'Saturdey'];
    let result;
    let daysUntilSpecDay = 0;
    let numberOfDay = 0;
    let today = new Date().getDay();

    for(let i = 0; i < days.length; i++){
        if(days[i] === day_name){
            numberOfDay = i;
        }
    }

    if(today < numberOfDay){
        daysUntilSpecDay = numberOfDay - today;
    }else{
        daysUntilSpecDay = 7 - (today - numberOfDay)
    }

    if(numberOfDay === today){
        result = 'Hey, today is ' + day_name + '=)';
    }else{
        result = "It's " + daysUntilSpecDay + ' day(s) left till '
            + day_name.charAt(0).toUpperCase() + day_name.slice(1);
    }

    return result;
}

//TASK 5.
function isValidIdentifier(some){
    let str = some;
    let regex = /((^[0-9])|\w([^a-z|A-z|0-9|_$]))/g;
    let result;

    if(regex.test(str)){
       result = false;
    }else{
        result = true;
    }

    return result;
}

//TASK 6.
function capitalize(some){
    let result;
    let str = some;
    let regex = /\b(\w)/g;
    result = str.replace(regex, c => c.toUpperCase());

    return result;
}

//TASK 7.
function isValidAudioFile(some){
    let result;
    let str = some;
    let regex1 =/\b[?<=.]mp3|flac|alac|aac/g;
    let regex2 = /[^a-zA-Z.0-9]+/g;
    if(regex1.test(str) && !regex2.test(str)){
        result = true;
    }else{
        result = false;
    }

    return result;
}

//TASK 8.
function getHexadecimalColors(some){
    let result = [];
    let str = some.split(' ');
    let regex = /.{0,4}[a-z];|.{0,6}[a-z0-9];/g;
    let j = 0;

    for(let i =0; i < str.length; i++){
        if(regex.test(str[i])){
            result[j] = str[i];
            j++;
        }
    }

    return result;
}

//TASK 9.
function isValidPassword(some){
    let result = false;
    let str = some;
    let regexUppercase = /[a-z]/g;
    let regexLowercase = /[A-Z]/g;
    let regexNumbers = /[0-9]/g;
    let regexLength = /.{8,}/g;

    if(regexUppercase.test(str) && regexLowercase.test(str) && regexNumbers.test(str) && regexLength.test(str)){
        result = true;
    }else{
        result = false;
    }

    return result;
}

//TASK 10.
function addThousandsSeparators(some){
    let str = some;
    let regex = /(\d)(?=(\d{3})+(?!\d))/g;
    let replace = '$1,';
    let string = String(str);

    return string.replace(regex, replace);
}
