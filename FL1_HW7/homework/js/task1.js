function hasWhiteSpace(s) {
    return s.indexOf(' ') >= 0;
}
function askBatteryAmmmount(){
    let amount_battery = prompt('Please enter the amount of batteries:');
    while(amount_battery <= 0 || isNaN(amount_battery) || hasWhiteSpace(amount_battery)){
        alert('Invalid input data: "' + amount_battery + '"');
        amount_battery = prompt('Please enter the amount of batteries:');
    }
    return amount_battery;
}
function askBatteryDefective(){
    let def_batt = prompt('Please enter the percentage of defective batteries:');
    let max_percentage = 100;
    while(def_batt <= 0 || def_batt > max_percentage || isNaN(def_batt) || hasWhiteSpace(def_batt)){
        alert('Invalid input data: "' + def_batt + '"');
        def_batt = prompt('Please enter the percentage of defective batteries:');
    }
    return def_batt;
}
function myBatteryCalculator() {
    let amount_battery = askBatteryAmmmount();
    let def_batt = askBatteryDefective();
    let max_percent = 100;
    let float_fix = 2;
    let amount_of_defective_battery = amount_battery / max_percent * def_batt;
    let amount_of_working_battery = amount_battery - amount_battery / max_percent * def_batt;
    alert('Amount of batteries: ' + amount_battery + '\n' +
        'Defective rate: ' + def_batt + '%\n' +
        'Amount of defective batteries: ' + amount_of_defective_battery.toFixed(float_fix) + '\n' +
        'Amount of working batteries: ' + amount_of_working_battery.toFixed(float_fix)
    );
}
