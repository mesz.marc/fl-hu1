function hasWhiteSpace(s) {
    return s.indexOf(' ') >= 0;
}
function inputWord(){
    let word = prompt('Please enter a word: ')
    while (word.length <= 0 || hasWhiteSpace(word) || !isNaN(word)){
        alert('Invalid value!');
        word = prompt('Please enter a word: ')
    }
    return word;
}
function findMiddleCalculator() {
    let word = inputWord();
    let two = 2;
    if(word.length % two === 0){
        if(word[word.length / two - 1] === word[word.length / two]){
            alert('Middle characters are the same!');
        } else {
            alert(word[word.length / two - 1] + word[word.length / two]);
        }
    } else {
        alert(word[Math.floor(word.length / two)]);
    }
}

