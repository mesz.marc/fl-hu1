/* START TASK 1: Your code goes here */
const table_cells = document.getElementsByTagName('td');
for(let i = 0; i < table_cells.length; i++){
    table_cells[i].addEventListener('click', e => {
        let target = e.target;
        if(target.id !== 'special_cell'){
            target.style.backgroundColor = 'yellow';
        }
    })
}
const first_cells = document.getElementsByClassName('first_cell');
for(let i = 0; i < first_cells.length; i++){
    first_cells[i].addEventListener('click', e => {
        let target = e.target;
        target.style.backgroundColor !== 'yellow'
        target.style.backgroundColor = 'blue';
            target = target.nextElementSibling;
        if(target.style.backgroundColor !== 'yellow') {
            target.style.backgroundColor = 'blue';
        }
            target = target.nextElementSibling;
        if(target.style.backgroundColor !== 'yellow') {
            target.style.backgroundColor = 'blue';
        }
    })
}
const specialCell = document.getElementById('special_cell');
specialCell.addEventListener('click', function() {
    let all_of_cells = document.getElementsByTagName('td');
    for(let i = 0; i < all_of_cells.length; i++){
        if(all_of_cells[i].style.backgroundColor !== 'yellow' && all_of_cells[i].style.backgroundColor !== 'blue'){
            all_of_cells[i].style.background = 'green';
        }
    }
})
/* END TASK 1 */

/* START TASK 2: Your code goes here */
document.getElementById('submit_button').disabled = true;
const phoneNumber = document.getElementById('phone_number');

phoneNumber.addEventListener('keyup', function() {
    let message = document.getElementById('message');
    const regex = new RegExp(/^[+]380\d+\d+\d+\d+\d+\d+\d+\d+\d+/g);
    console.log(regex.test(phoneNumber.value));
    if(!regex.test(phoneNumber.value) && phoneNumber.value.length !== 13){
        message.innerHTML = "Type number doesn't follow format +380*********";
        message.classList.add('error');
        document.getElementById('phone_number').style.borderColor = 'red';
        document.getElementById('submit_button').disabled = true;
    } else {
        document.getElementById('submit_button').disabled = false;
        document.getElementById('phone_number').style.borderColor = 'revert';
        message.style.display = 'none';
    }
})
const submitPhoneNumber = document.getElementById('submit_button');
submitPhoneNumber.addEventListener('click', function() {
    let message = document.getElementById('message');
    message.style.display = 'block';
    message.innerHTML = 'Data was successfully sent';
    message.classList.remove('error');
    message.classList.add('success');
    document.getElementById('phone_number').style.borderColor = 'revert';
})
/* END TASK 2 */

/* START TASK 3: Your code goes here */
let opacity = 0;
function hide(){
    let notification = document.getElementById('notification');
    opacity = Number(window.getComputedStyle(notification).getPropertyValue('opacity'));
    if(opacity > 0){
        opacity = opacity - 0.1;
        notification.style.opacity = opacity
    }
}
const basketballGame = document.getElementById('court');
let score_a = 0;
let score_b = 0;
basketballGame.addEventListener('click', e => {
    let x = e.offsetX - 300;
    let y = e.offsetY - 20;
    console.log(x);
    console.log(y);
    document.getElementById('ball').style.left = x + 'px';
    document.getElementById('ball').style.top = y + 'px';
    if (y > 136 && y < 152 && x < -252 && x > -268) {
        score_b = score_b + 1;
        document.getElementById('result_b').innerHTML = 'Team B: ' + score_b;
        document.getElementById('notification').innerHTML = 'Team B score!';
        document.getElementById('notification').classList.remove('a_score_class');
        document.getElementById('notification').classList.add('b_score_class');
        document.getElementById('notification').style.opacity = '1';
        setInterval(hide, 300);
    } else if (y > 136 && y < 152 && x < 266 && x > 250) {
        score_a = score_a + 1;
        document.getElementById('result_a').innerHTML = 'Team A: ' + score_a;
        document.getElementById('notification').innerHTML = 'Team A score!';
        document.getElementById('notification').classList.remove('b_score_class');
        document.getElementById('notification').classList.add('a_score_class');
        document.getElementById('notification').style.display = 'block';
        document.getElementById('notification').style.opacity = '1';
        setInterval(hide, 300);
    }
})
/* END TASK 3 */
