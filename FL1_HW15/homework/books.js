let books = [
    {
        'id': '1',
        'title': 'JavaScript: The Good Parts',
        'author': 'Douglas Crockford',
        'image': 'https://images-na.ssl-images-amazon.com/images/I/5131OWtQRaL._SX218_BO1,204,203,200_QL40_FMwebp_.jpg',
        'plot': 'With the JavaScript: The Good Parts, author Douglas Crockford focuses on the basics of some of the ' +
            'lesser-known yet desirable aspects of JavaScript. It’s only recently that these hidden features are ' +
            'getting the appreciation they deserve from the programming community.'
    },
    {
        'id': '2',
        'title': 'JavaScript: The Good Parts2',
        'author': 'Douglas Crockford2',
        'image': 'https://images-na.ssl-images-amazon.com/images/I/5131OWtQRaL._SX218_BO1,204,203,200_QL40_FMwebp_.jpg',
        'plot': 'With the JavaScript: The Good Parts, author Douglas Crockford focuses on the basics of some of the ' +
            'lesser-known yet desirable aspects of JavaScript. It’s only recently that these hidden features are ' +
            'getting the appreciation they deserve from the programming community.'
    },
    {
        'id': '3',
        'title': 'JavaScript: The Good Parts3',
        'author': 'Douglas Crockford3',
        'image': 'https://images-na.ssl-images-amazon.com/images/I/5131OWtQRaL._SX218_BO1,204,203,200_QL40_FMwebp_.jpg',
        'plot': 'With the JavaScript: The Good Parts, author Douglas Crockford focuses on the basics of some of the ' +
            'lesser-known yet desirable aspects of JavaScript. It’s only recently that these hidden features are ' +
            'getting the appreciation they deserve from the programming community.'
    }
];
localStorage.setItem('booksInfo', JSON.stringify(books));