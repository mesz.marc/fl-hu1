const root = document.getElementById('root');
function updateBooks(id, href){
    let books = JSON.parse(localStorage.getItem('booksInfo')) || [];
    let newBookName = document.getElementById('bname').value;
    let newBookAuthor = document.getElementById('author').value;
    let newBookImage = document.getElementById('imageurl').value;
    let newBookPlot = document.getElementById('plot').value;

    for(let i in books){
        if(parseInt(books[i].id) === id){
            books[i].title = newBookName;
            books[i].author = newBookAuthor;
            books[i].image = newBookImage;
            books[i].plot = newBookPlot;
        }
    }
    window.location.href = href;
    localStorage.setItem('booksInfo', JSON.stringify(books));
    listingBooks();
    previewBooks(id);
    setTimeout(succesfulUpdate(), 200);
}
function succesfulUpdate(){
    let modal = document.getElementById('myModal');
    modal.innerHTML = '  <div class="modal-content">' +
        '<span class="close">&times;</span>' +
        '<p>Book successfully updated</p>' +
        '</div>';

    modal.style.display = 'block';
    let span = document.getElementsByClassName('close')[0];
    span.onclick = function() {
        modal.style.display = 'none';
    }
    window.onclick = function(event) {
        if (event.target === modal) {
            modal.style.display = 'none';
        }
    }
}

function cancelUpdate(){
    let modal = document.getElementById('myModal');
    modal.innerHTML = '<div class="modal-content">' +
        '<span class="close">&times;</span>' +
        '<p>Discard changes?</p>' +
        '<button id="goBack" type="button">Confirm</button>' +
        '<button id="cancelDiscardChanges" type="button">Cancel</button>' +
        '</div>';

    modal.style.display = 'block';

    let span = document.getElementsByClassName('close')[0];
    let button = document.getElementById('goBack');
    let cancel = document.getElementById('cancelDiscardChanges');
    span.onclick = function() {
        modal.style.display = 'none';
    }
    button.onclick = function(){
        window.history.back();
        document.getElementById('bookPreview').innerHTML = '';
        modal.style.display = 'none';
    }
    cancel.onclick = function(){
        modal.style.display = 'none';
    }
    window.onclick = function(event) {
        if (event.target === modal) {
            modal.style.display = 'none';
        }
    }
}
function validateForm(some){
    let bname = document.forms[some]['bname'].value;
    let author = document.forms[some]['author'].value;
    let imageurl = document.forms[some]['imageurl'].value;
    let plot = document.forms[some]['plot'].value;

    if(bname === ''){
        alert('Book name can not be empty!');
        return false;
    }else if(author === ''){
        alert('Author can not be empty!');
        return false;
    }else if(imageurl === ''){
        alert('Image url can not be empty!');
        return false;
    }else if(plot === ''){
        alert('Plot can not be empty!');
        return false;
    }else{
        return true;
    }
}
function listingBooks(){
    let books = JSON.parse(localStorage.getItem('booksInfo')) || [];
    let result = document.getElementById('booksList');
    result.innerHTML = '';
    for(let i in books){
        result.innerHTML += '<li><span onclick="previewBooks('+ books[i].id + ')">' + books[i].title + '</span>' +
            '<button id="edit_button" value="" onclick="editBooks(' + books[i].id +')">Edit</button>' +
            '</li>';
    }
    return result;
}

function listingBooksSection(){
    let result = '<div id="listofBooks">';
    result += '<h1>BOOKS</h1>';
    result += '<ul id="booksList">';
    result += '</ul>';
    result += '<button id="add_button" onclick="addNewBook()">+</button>';
    result += '</div>';
    return result;
}
function editBooksSection(){
    let result = '<div id="editBooks">';
    result += '<div id="bookPreview"></div>';
    result += '</div>';
    result += '<div id="myModal" class="modal">';
    return result;
}

function previewBooks(some){
    let books = JSON.parse(localStorage.getItem('booksInfo')) || [];
    let result = document.getElementById('bookPreview');
    let id = parseInt(some);
    window.history.pushState({id}, 'Title', '?id=' + id + '#preview');
    for(let i in books){
        if(parseInt(books[i].id) === some){
            result.innerHTML = '<div class="preview"><h1>' + books[i].title + '</h1>' +
                            '<span>Author: </span><p>' + books[i].author + '</p>' +
                            '<img src="' + books[i].image + '">' +
                            '<span>Plot: </span><p>' + books[i].plot + '</p></div>';
        }
    }
}

function saveBooks(){
    let bname = document.forms['bookSave']['bname'].value;
    let author = document.forms['bookSave']['author'].value;
    let imageurl = document.forms['bookSave']['imageurl'].value;
    let plot = document.forms['bookSave']['plot'].value;

    let books = JSON.parse(localStorage.getItem('booksInfo')) || [];
    let id = books.length + 1;
    id = String(id);
    books.push({'id': id, 'title': bname, 'author': author, 'image': imageurl, 'plot': plot});
    localStorage.setItem('booksInfo', JSON.stringify(books));
    listingBooks();
    previewBooks(id);
}

function addNewBook(){
    let books = JSON.parse(localStorage.getItem('booksInfo')) || [];
    let id = books.length + 1;
    window.onload
    window.history.pushState({id}, 'Title', '#add');
    let result = document.getElementById('bookPreview');
    result.innerHTML = '<h1>New Book</h1>' +
        '<form name="bookSave">' +
        '  <label for="bname">Book name:</label>' +
        '  <input type="text" id="bname" name="bname" value="" ><br><br>' +
        '  <label for="author">Author:</label>' +
        '  <input type="text" id="author" name="author" value="" ><br><br>' +
        '  <label for="imageurl">Image url:</label>' +
        '  <input type="text" id="imageurl" name="imageurl" value="" ><br><br>' +
        '  <label for="plot">Plot:</label>' +
        '  <textarea id="plot" name="plot" rows="8" cols="50"></textarea><br><br>' +
        '</form>' +
        '<div id="submitButton"><button id="saveButton" type="submit">Save</button>' +
        '<button type="submit" onclick="cancelUpdate()">Cancel</button</div>';

    let saveButton = document.getElementById('saveButton');
    saveButton.onclick = function () {
        if(validateForm('bookSave')){
            saveBooks();
        }
    }
}
function editBooks(some){
    let result = document.getElementById('bookPreview');
    let books = JSON.parse(localStorage.getItem('booksInfo')) || [];
    let id = parseInt(some);
    window.history.pushState({id}, 'Title', '?id=' + id + '#edit');
    for(let i in books){
        if(parseInt(books[i].id) === some){
            result.innerHTML = '<h1>' + books[i].title + '</h1>' +
                        '<form name="bookSubmit">' +
                        '  <label for="bname">Book name:</label>' +
                        '  <input type="text" id="bname" name="bname" value="'+ books[i].title +'" ><br><br>' +
                        '  <label for="author">Author:</label>' +
                        '  <input type="text" id="author" name="author" value="'+ books[i].author +'" ><br><br>' +
                        '  <label for="imageurl">Image url:</label>' +
                        '  <input type="text" id="imageurl" name="imageurl" value="'+ books[i].image +'" ><br><br>' +
                        '  <label for="plot">Plot:</label>' +
                        '  <textarea id="plot" name="plot" rows="8" cols="50">'+ books[i].plot +'</textarea><br><br>' +
                        '</form>' +
                        '<div id="submitButton"><button id="updateButton" type="submit">Save</button>' +
                        '<button type="submit" onclick="cancelUpdate()">Cancel</button</div>';
        }
    }
    let updateButton = document.getElementById('updateButton');
    updateButton.onclick = function () {
        if(validateForm('bookSubmit')){
            updateBooks(some, window.location.href);
        }
    }
    return result;
}
root.innerHTML += listingBooksSection();
root.innerHTML += editBooksSection();
listingBooks();


