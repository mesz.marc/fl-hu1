// 1. Function to convert numbers to string and wise versa
function convert(){
    let convertArray = [];
    for(let i = 0; i < arguments.length; i++){
        if(typeof arguments[i] === 'string'){
            convertArray[i] = parseInt(arguments[i]);
        }else{
            convertArray[i] = String(arguments[i]);
        }
    }

    return convertArray;
}
// 2.  Function which iterates over array and executes function on each element
function executeforEach(arr, myCallBack){
    let array = arr;
    let i;
    let j = 0;
    let result = [];
    let two = 2;
    for(i = 0; i < array.length; i++){
        if(typeof myCallBack(array[i]) !== 'undefined'){
            result[j] = myCallBack(array[i]);
            j++;
        }
    }
    if(myCallBack === getTotalWeight){
        result = weight / two;
    }
    weight = 0;

    return result;
}
// 3. Function which returns transformed array based on function...
// Example of usage of this function: executeforEach([2, '5', 8], mapArray);
function mapArray(some){
    let convertChar = some;
    let addNum = 3;
    if(typeof convertChar === 'string' ){
        convertChar = parseInt(convertChar)
        convertChar += addNum;
    }else{
        convertChar = convertChar + addNum;
    }

    return convertChar;
}

// 4. Function, which returns filtered array based on function, which passed as a parameter.
// Example of usage of this function: executeforEach([2, 5, 8], filterArray);
function filterArray(some){
    let filteredNumber;
    let two = 2;
    if(some % two === 0){
        filteredNumber = some;
    }

    return filteredNumber;
}
// 5. A Function that takes an array and a value as arguments and returns the value position
function getValuePosition(some, a){
    let valueArr = some;
    let position = a;
    let result;
    for(let i = 0; i < valueArr.length; i++){
        if(valueArr[i] === position){
            result = i+1;
            break;
        }else{
            result = false;
        }
    }

    return result;
}
// 6. A Function that reverses the string value passed into it
function flipOver(some){
    let str = some;
    let result = '';
    for(let i = str.length - 1; i >= 0; i--){
        result += str[i];
    }

    return result;
}
// 7. Function Generate numbers from the given range
function makeListFromRange(a, b){
    let i;
    let j = 0;
    let result = [];
    for(i = a; i <= b; i++){
        result[j] = i;
        j++;
    }

    return result;
}
// 8. A function that accepts an array of object and returns new array of values by passed key name
// Example of usage of this function: executeforEach(fruits, getArrayOfKeys)
//const fruits = [{ name: "apple", weight: 0.5 },{ name: "pineapple", weight: 2 }];
function getArrayOfKeys(some){
    let arr = some;
    let result = arr.name;

    return result;
}
//9. A Function that accepts an array of groceries objects and returns total weight of all items
// Example of usage of this function: executeforEach(basket, getTotalWeight)
//const basket = [{ name: "Bread", weight: 0.3 }, { name: "Coca-Cola", weight: 0.5 }, { name: "Watermelon", weight: 8 }];
let weight = 0;
function getTotalWeight(some){
    let arr = some;
    weight += arr.weight;

    return weight;
}
//10. A Function which returns a day number that was some amount of days ago from the passed date
//const date = new Date(2020, 0, 2);
function getPastDay(some, a){
    let d = new Date(some),
        dateTime = d.getTime(),
        numberOfDays = a,
        milliseconds = 1000,
        seconds = 60,
        minutes = 60,
        hours = 24,
        day = milliseconds * seconds * minutes * hours,
        result;
    result = new Date(dateTime - numberOfDays * day).getDate();

    return result;
}
//11. A Function formats a date in such format "YYYY/MM/DD HH:mm"
//const date = new Date('6/15/2019 09:15:00');
function formatDate(some) {
    let d = new Date(some),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear(),
        hour = d.getHours(),
        minutes = d.getMinutes(),
        two = 2;
    let result;
    if (month.length < two){
        month = '0' + month;
    }
    if (day.length < two){
        day = '0' + day;
    }
    if (hour.length < two){
        hour = '0' + day;
    }
    if (minutes.length < two){
        minutes = '0' + day;
    }
    result = year + '/' + month + '/' + day + ' ' + hour + ':' + minutes;

    return result;
}