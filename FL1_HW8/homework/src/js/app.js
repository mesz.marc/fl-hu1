/*global questions*/
let question_number = 0;
let current_prize = 100;
let total_prize = 0;
let correct_answer = '';
let askedQuestionArr = [];
let questionArrayi = 0;
let skippedQuestion = 0;
let maxPrize = 1000000;
let numberOfAnswers = 4;

//If the game ends this function will show the result
function endOfGame(){
    document.getElementById('current_prize').style.display = 'none';
    document.getElementById('total_prize').style.display = 'none';
    document.getElementById('question').style.display = 'none';
    document.getElementById('answers').style.display = 'none';
    document.getElementById('end_game_message').style.display = 'block';
    if(total_prize >= maxPrize){
        document.getElementById('end_game_message').innerHTML = 'Congratulation your prize: ' + maxPrize;
    }else{
        document.getElementById('end_game_message').innerHTML = 'Game over. Your Prize is: ' + total_prize;
    }
}
//Checking the answer is it correct or not
function isAnswerCorrect(button_result) {
    let answer = button_result;
    if(answer !== correct_answer){
        endOfGame();
    }else{
        total_prize += current_prize;
        current_prize += current_prize;
        if(total_prize >= maxPrize){
            endOfGame();
        }else{
            getQuestion();
        }
    }
}
//The selected answer button onclick method call this
function returnButtonValue(buttonValue) {
    let button_result = buttonValue;
    isAnswerCorrect(button_result);
    return button_result;
}
//Checking the question if it has been asked before
function isQuestionAsked(a){
    let question = a;
    let isAsked = false;
    let i = 0;
    let arrLength = askedQuestionArr.length;
    do{
        if(question === askedQuestionArr[i]){
           isAsked = true;
        }
        i++;
    } while(i < arrLength)
    return isAsked;
}
//Generate the questions and answers
function getQuestion() {
    let lengthOfQuestionArr = questions.length;
    question_number = Math.floor(Math.random() * lengthOfQuestionArr);
    let question = questions[question_number].question;
    document.getElementById('question').innerHTML = question;

    if(isQuestionAsked(question) === false){
        askedQuestionArr[questionArrayi] = question;
        questionArrayi++;
        document.getElementById('answers').innerHTML = '';
        for(let i = 0; i < numberOfAnswers; i++){
            document.getElementById('answers').innerHTML += '<input type="button" '
            + 'class="answer_button btn btn-outline-secondary" '
            + 'onclick="return returnButtonValue(this.value)" value="'
            + questions[question_number].content[i] + '"><br>';
        }
        document.getElementById('current_prize').innerHTML = 'Prize on current round: ' + current_prize;
        document.getElementById('total_prize').innerHTML = 'Total prize: ' + total_prize;
        correct_answer = questions[question_number].content[questions[question_number].correct];
    }else{
        getQuestion();
    }
}
//Skip question function
function skipQuestion(){
    if(skippedQuestion < 1){
        getQuestion();
        document.getElementById('skipped_question').style.display = 'none';
        skippedQuestion++;
    }
}
//Function to start the game
function startGame(){
    let hundred = 100;
    document.getElementById('current_prize').style.display = 'block';
    document.getElementById('total_prize').style.display = 'block';
    document.getElementById('question').style.display = 'block';
    document.getElementById('answers').style.display = 'block';
    document.getElementById('end_game_message').style.display = 'none';
    document.getElementById('skipped_question').style.display = 'block';
    document.getElementById('skipped_question').style.textAlign = 'center';
    question_number = 0;
    current_prize = hundred;
    total_prize = 0;
    correct_answer = '';
    askedQuestionArr = [];
    questionArrayi = 0;
    skippedQuestion = 0;
    getQuestion();
}
