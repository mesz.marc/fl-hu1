const data = [
  {
    'folder': true,
    'title': 'Grow',
    'children': [
      {
        'title': 'logo.png'
      },
      {
        'folder': true,
        'title': 'English',
        'children': [
          {
            'title': 'Present_Perfect.txt'
          }
        ]
      }
    ]
  },
  {
    'folder': true,
    'title': 'Soft',
    'children': [
      {
        'folder': true,
        'title': 'NVIDIA',
        'children': null
      },
      {
        'title': 'nvm-setup.exe'
      },
      {
        'title': 'node.exe'
      }
    ]
  },
  {
    'folder': true,
    'title': 'Doc',
    'children': [
      {
        'title': 'project_info.txt'
      }
    ]
  },
  {
    'title': 'credentials.txt'
  }
];

const rootNode = document.getElementById('root');

function deleteElement(id){
    id.style.display = "none";
}

function editElement(id){
    let editName = id.getElementsByTagName("p");
    editName[0].innerHTML = '<input type="text" id="name">';
}

function contextMenu(event, id){
    const documentClickHandler = function(e) {
        const isClickedOutside = !menu.contains(e.target);
        if (isClickedOutside) {
            menu.classList.add('hidden');
            document.removeEventListener('click', documentClickHandler);
        }
    }

    const element_id = id;
    let element = document.getElementById("contmenu");

    element.innerHTML = '<ul id="menu" ' +
        'class="absolute bg-gray-100 border border-gray-400 shadow-xl rounded hidden">\n' +
        '                <li class="px-2 py-1 whitespace-no-wrap hover:bg-blue-200" ' +
        'onclick="editElement(' + element_id + ')">Edit</li>\n' +
        '                <li class="px-2 py-1 whitespace-no-wrap hover:bg-blue-200" ' +
        'onclick="deleteElement(' + element_id + ')">Delete</li>\n' +
        '</ul>';

    const menu = document.getElementById('menu');
    const ele = document.getElementById(element_id);

    event.preventDefault();

    const rect = ele.getBoundingClientRect();
    const x = event.clientX - rect.left + 50;
    const y = event.clientY - rect.top;

    menu.style.top = `${y}px`;
    menu.style.left = `${x}px`;

    menu.classList.remove('hidden');
    menu.classList.add('show');

    document.addEventListener('click', documentClickHandler);

}

function changeIcon(some){
    let element = document.getElementById(some);
    if(element.innerHTML === "folder_open"){
        element.innerHTML = "folder";
    }else{
        element.innerHTML = "folder_open";
    }

}

function showSubItems(number, name){
    let subname = name.trim().toLowerCase() + '_' + number;
    let subClassName = document.getElementsByClassName(subname);

    for(let i = 0; i < subClassName.length; i++){
        if(subClassName[i].style.display === "block"){
            subClassName[i].style.display = "none";
        }else{
            subClassName[i].style.display = "block";
        }
    }

}

function listingFolders(){
    let list = '<div class="relative"><ul class="list">';

  for(let i = 0; i < data.length; i++){
    if(new RegExp(/\.+.../g).test(data[i].title)){
        list += '<li id="file_' + i + '" oncontextmenu="contextMenu(event, \'file_' + i + '\')" ' +
            'class="files"><i class="material-icons file">insert_drive_file</i><p>' + data[i].title + '</p></li>';
    }else{
      list += "<li id='fold_" + i + "' oncontextmenu='contextMenu(event, \"fold_" + i + "\")' " +
          "onclick='showSubItems(" + i + ", \"" + data[i].title + "\");changeIcon(\"" + data[i].title + "\")'  " +
          "class='folders'><i id='" + data[i].title + "' class='material-icons folder'>folder</i>" +
          "<p>" + data[i].title + "</p></li>";

        if(data[i].children){
            list += '<ul class="sublist ' + data[i].title.toLowerCase() + "_" + i + ' ">'
            for(let j = 0; j < data[i].children.length; j++){
                if(new RegExp(/\.+.../g).test(data[i].children[j].title)){
                    list += '<li id="subfile_' + j + '" oncontextmenu="contextMenu(event, \'subfile_' + j + '\')" ' +
                        'class="subfile"><i class="material-icons file">insert_drive_file</i>' +
                        '<p>' + data[i].children[j].title + '</p></li>';
                }else{
                    list += "<li id='subfold_" + j + "' oncontextmenu='contextMenu(event, \"subfold_" + j + "\")' " +
                        "onclick='showSubItems(" + j + ", \"" + data[i].children[j].title+ "\");" +
                        "changeIcon(\"" + data[i].children[j].title + "\")' " +
                        "class='subfolder'><i id='" + data[i].children[j].title + "'" +
                        "class='material-icons folder'>folder</i>" +
                        "<p>" + data[i].children[j].title + "</p></li>";

                    if(data[i].children[j].children){
                        list += '<ul class="subsublist ' + data[i].children[j].title.toLowerCase() + "_" + j + '">'
                        for(let k = 0; k < data[i].children[j].children.length; k++){
                            if(new RegExp(/\.+.../g).test(data[i].children[j].children[k].title)){
                                list += '<li id="subsubfile_' + k + '" ' +
                                    'oncontextmenu="contextMenu(event, \'subsubfile_' + k + '\')" ' +
                                    'class="subsubfile"><i class="material-icons file">insert_drive_file</i>' +
                                    '<p>' + data[i].children[j].children[k].title + '</p></li>';
                            }else{
                                list += '<li id="subsubfold_' + k + '" ' +
                                    'oncontextmenu="contextMenu(event, \'subsubfold_' + k + '\')" ' +
                                    'class="subsubfolder"><i class="material-icons folder">folder</i>' +
                                    '<p>' + data[i].children[j].children[k].title + '</p></li>';
                            }
                        }
                        list += '</ul>';
                    }else{
                        list += '<ul class="subsublist ' + data[i].children[j].title.toLowerCase() + "_" + j + '">'
                        list += '<li class="subsubfolder empty">Folder is empty!</li>';
                        list += '</ul>';
                    }
                }
            }
            list += '</ul>';
        }
    }
  }
    list += '</ul>';
    list += '<div id="contmenu"></div>';
    list += '</div>'
    rootNode.innerHTML = list;
    return rootNode;
}

listingFolders();
