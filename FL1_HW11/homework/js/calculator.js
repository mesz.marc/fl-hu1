function calculator(){
    let result = 0;
    let calculation = prompt('Enter the numbers and the operation below:');
    let nums = [];
    let num1 = '';
    let num2 = '';

    if(calculation.includes('+')){
        nums = calculation.split('+');
        num1 = parseInt(nums[0]);
        num2 = parseInt(nums[1]);
        result = num1 + num2;
    } else if(calculation.includes('-')) {
        nums = calculation.split('-');
        num1 = parseInt(nums[0]);
        num2 = parseInt(nums[1]);
        result = num1 - num2;
    } else if(calculation.includes('*')) {
        nums = calculation.split('*');
        num1 = parseInt(nums[0]);
        num2 = parseInt(nums[1]);
        result = num1 * num2;
    } else if(calculation.includes('/')) {
        nums = calculation.split('/');
        num1 = parseInt(nums[0]);
        num2 = parseInt(nums[1]);
        result = num1 / num2;
    }else{
        result = 'No operation';
    }
    try{
        if(isNaN(result) === true) {
            throw 'The result is not a number';
        }
        if(num1 === '') {
            throw 'First number empty!';
        }
        if(num2 === '') {
            throw 'Second number empty!';
        }
        if(result === 'No operation') {
            throw 'There is no operation in the given string!';
        }
        if(isNaN(num1)) {
            throw 'First number not a number!';
        }
        if(isNaN(num2)) {
            throw 'Second number not a number!';
        }
    } catch(err){
        alert(err);
        calculator();
    }
    if(isNaN(result)){
        alert('Thanks!')
    }else{
        alert(result);
    }
}

calculator();
