function visitLink(path) {
    let already_visited;
    if(path === 'Page1'){
        already_visited = localStorage.getItem('visitedLink_1');
        already_visited++;
        localStorage.setItem('visitedLink_1', already_visited);
    } else if (path === 'Page2'){
        already_visited = localStorage.getItem('visitedLink_2');
        already_visited++;
        localStorage.setItem('visitedLink_2', already_visited);
    } else if (path === 'Page3'){
        already_visited = localStorage.getItem('visitedLink_3');
        already_visited++;
        localStorage.setItem('visitedLink_3', already_visited);
    }
}

function viewResults() {
    if(document.getElementById('result') !== null){
        document.getElementById('result').style.display = 'none';
    }
    let target = document.getElementsByClassName('btn');
    target = target[0];
    target.insertAdjacentHTML('afterend', '<ul id="result"><li>You visited Page3 ' +
        localStorage.getItem("visitedLink_3") + ' time(s)</li>' +
        '<li>You visited Page1 ' + localStorage.getItem("visitedLink_1") + ' time(s)</li>' +
        '<li>You visited Page2 ' + localStorage.getItem("visitedLink_2") + ' time(s)</li></ul>');
}